/**
 * A more friendly server that responds with an HTML greeting
 */
const http = require("http") 
const port = 8082

const server = http.createServer((req, res) => {
  res.writeHead(200, { 'Content-Type': 'text/html' })
  res.write("<!doctype html>")
  res.write("<html><head><title>Server2</title></head>")
  res.write("<body>")
  res.write("<h1>Hello Client!</h1>")
  res.write("<p>This is a simple web server built with Node.js</p>")
  res.write("<p>Right-click and 'View Source' to see the html</p>")
  res.write("<p>When you start with node, you'll have to restart the server if you make changes.</p>")
  res.write("<p>If this gets old, look up 'nodemon' (short for node monitor) and start your server with nodemon instead.</p>")
  res.end("</body></html>")
})

server.listen(port, (err) => {
  if (err) { return console.log('Error', err) }
  console.log(`Server listening at http://127.0.0.1:${port}`)
})

// create the server using anonymous arrow functions
// The arrow function is concise. (Arguments) => {logic}
// abbreviate request and response 
// return html
// start listening and handle errors
// pass in port and provide an arrow function for errors
// How do we call this? How do we make a request?
// How do we see our app?  What will appear? 
// Does this ever change?