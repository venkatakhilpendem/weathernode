/**
 * A module with a several functions that request weather for given city.
 */
const http = require('http');

const api = {
    getTemp: async function get(city) {
        return new Promise(function (resolve, reject) {
            const url = 'http://api.openweathermap.org/data/2.5/weather?q=' + city + '&units=metric&apikey=c184205bc1fcbcdc42c4b37ccf710de3'
            const req = http.get(url, (res) => {
                let body = '';
                res.on('data', function (chunk) { body += chunk; }); // On getting repsonse data, do this
                res.on('end', function () {         // when done getting response data, do this
                    const o = JSON.parse(body)
                    if (o.name === undefined || o.main === undefined || o.main.temp === undefined) {
                        resolve('Invalid request')
                        return
                    }
                    const s = 'In ' + o.name + ', temp is ' + o.main.temp + ' degrees C.'
                    console.log(s)
                    resolve(s)
                })
            })
            req.on('error', (err) => reject(err.message))
        })
    },
    getHumidity: async function get(city) {
        return new Promise(function (resolve, reject) {
            const url = 'http://api.openweathermap.org/data/2.5/weather?q=' + city + '&units=metric&apikey=c184205bc1fcbcdc42c4b37ccf710de3'
            const req = http.get(url, (res) => {
                let body = '';
                res.on('data', function (chunk) { body += chunk; }); // On getting repsonse data, do this
                res.on('end', function () {         // when done getting response data, do this
                    const o = JSON.parse(body)
                    if (o.name === undefined || o.main === undefined || o.main.temp === undefined) {
                        resolve('Invalid request')
                        return
                    }
                    s = 'In ' + o.name + ', humidity is ' + o.main.humidity + ' percent'
                    console.log(s)
                    resolve(s)
                })
            })
            req.on('error', (err) => reject(err.message))
        })
    }
}

module.exports = api;
